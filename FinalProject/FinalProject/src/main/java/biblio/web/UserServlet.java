package biblio.web;

import biblio.service.UserService;
import biblio.transfer.LongWrapper;
import biblio.transfer.RegisterUserDTO;
import biblio.transfer.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * insert comment here
 */

@RestController
public class UserServlet {

    @Autowired
    UserService userService;

    @RequestMapping("/user")
    public Principal user(Principal user) {
        System.out.println(user);
        return user;
    }

    @RequestMapping("/users/getAll")
    public List<UserDTO> getAllUsers() {
        return userService.loadAllUsers();
    }

    @RequestMapping(value = "/users/getUser", method = RequestMethod.POST)
    public
    @ResponseBody
    UserDTO getUser(@RequestBody LongWrapper id) {
        UserDTO userDTO = userService.getUserById(id.getId());
        return userDTO;
    }

    @RequestMapping(value = "/users/removeUser", method = RequestMethod.POST)
    public
    @ResponseBody
    boolean removeUser(@RequestBody LongWrapper id) {
        userService.removeUser(id.getId());
        return true;
    }

    @RequestMapping(value = "/users/editUser", method = RequestMethod.POST)
    public
    @ResponseBody
    boolean editUser(@RequestBody UserDTO user) {
        userService.editUser(user);
        return true;
    }

    @RequestMapping(value = "/users/register", method = RequestMethod.POST)
    public
    @ResponseBody
    boolean registerUser(@RequestBody RegisterUserDTO user) {
        userService.createUser(user);
        return true;
    }

}
