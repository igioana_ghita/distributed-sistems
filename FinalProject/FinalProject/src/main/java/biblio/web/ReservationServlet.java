package biblio.web;

import biblio.model.User;
import biblio.service.ReservationService;
import biblio.service.UserService;
import biblio.transfer.ReservationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * insert comment here
 */
@RestController
public class ReservationServlet {

    @Autowired
    UserService userService;

    @Autowired
    ReservationService reservationService;

    @RequestMapping("/reservations/getForUser")
    public List<ReservationDTO> getReservationsForLoggedInUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.getUserByUsername(auth.getName());
        return reservationService.getReservationsForUser(user);
    }
}
