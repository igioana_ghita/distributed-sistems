package biblio.web;

import biblio.model.User;
import biblio.service.BookService;
import biblio.service.UserService;
import biblio.transfer.BookDTO;
import biblio.transfer.LongWrapper;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;

/**
 * insert comment here
 */
@RestController
public class BookServlet {

    @Autowired
    BookService bookService;

    @Autowired
    UserService userService;

    @RequestMapping(value = "/books/getAll", method = RequestMethod.POST)
    public
    @ResponseBody
    List<BookDTO> getAllBooks(@RequestBody BookDTO filter) {
        return bookService.loadAllBooks(filter);
    }

    @RequestMapping(value = "/download", method = RequestMethod.POST)
    public void download(@RequestBody BookDTO filter, final HttpServletRequest request, final HttpServletResponse response) throws FileNotFoundException {

        bookService.exportCsv(filter);
        File file = new File ("src/main/resources/export.csv" );
        try (InputStream fileInputStream = new FileInputStream(file);
             OutputStream output = response.getOutputStream()) {

            response.reset();

            response.setContentType("application/octet-stream");
            response.setContentLength((int) (file.length()));

            response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");

            IOUtils.copyLarge(fileInputStream, output);
            output.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @RequestMapping(value = "/books/insertBook", method = RequestMethod.POST)
    public boolean insertBook(@RequestBody BookDTO bookDTO) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.getUserByUsername(auth.getName());
        bookService.insertBook(bookDTO, user);
        return true;
    }

    @RequestMapping(value = "/books/editBook", method = RequestMethod.POST)
    public boolean editBook(@RequestBody BookDTO bookDTO) {
        bookService.editBook(bookDTO);
        return true;
    }

    @RequestMapping(value = "/books/getBook", method = RequestMethod.POST)
    public
    @ResponseBody
    BookDTO getBook(@RequestBody LongWrapper id) {
        BookDTO bookDTO = bookService.getBook(id.getId());
        return bookDTO;
    }

    @RequestMapping(value = "/books/removeBook", method = RequestMethod.POST)
    public
    @ResponseBody
    boolean removeBook(@RequestBody LongWrapper id) {
        bookService.removeBook(id.getId());
        return true;
    }

    @RequestMapping(value = "/books/bookBook", method = RequestMethod.POST)
    public
    @ResponseBody
    boolean bookBook(@RequestBody LongWrapper id) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.getUserByUsername(auth.getName());
        bookService.bookBook(id.getId(), user);
        return true;
    }

    @RequestMapping(value = "/books/returnBook", method = RequestMethod.POST)
    public
    @ResponseBody
    boolean returnBook(@RequestBody LongWrapper id) {
        bookService.returnBook(id.getId());
        return true;
    }

}
