package biblio.model;

import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 * insert comment here
 */

@Entity
public class Membership {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "member_seq")
    @SequenceGenerator(name = "member_seq", sequenceName = "member_seq", allocationSize = 1)
    private long id;

    private String password;

    @Size(max = 5000)
    private byte[] encryptedPassword;

    @Size(max = 5000)
    private byte[] salt;

    private int algorithm;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(int algorithm) {
        this.algorithm = algorithm;
    }

    public byte[] getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(byte[] encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }

    public byte[] getSalt() {
        return salt;
    }

    public void setSalt(byte[] salt) {
        this.salt = salt;
    }
}
