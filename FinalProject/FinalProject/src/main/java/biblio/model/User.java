package biblio.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * insert comment here
 */
@Entity
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "user_seq")
    @SequenceGenerator(name = "user_seq", sequenceName = "user_seq", allocationSize = 1)
    private long id;

    @Column(unique = true)
    private String username;

    @Column(nullable = false)
    private String email;

    private Date birthDate;

    private String address;

    @Column(nullable = false)
    private int type;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "membership_fk")
    private Membership membership;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "creator_fk")
    private List<Book> createdBooks;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "booker_fk")
    private List<Reservation> bookings;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Membership getMembership() {
        return membership;
    }

    public void setMembership(Membership membership) {
        this.membership = membership;
    }

    public List<Book> getCreatedBooks() {
        return createdBooks;
    }

    public void setCreatedBooks(List<Book> createdBooks) {
        this.createdBooks = createdBooks;
    }

    public List<Reservation> getBookings() {
        return bookings;
    }

    public void setBookings(List<Reservation> bookings) {
        this.bookings = bookings;
    }
}

