package biblio.model;

import javax.persistence.*;
import java.util.Date;

/**
 * insert comment here
 */

@Entity
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "reser_seq")
    @SequenceGenerator(name = "reser_seq", sequenceName = "reser_seq", allocationSize = 1)
    private long id;

    private Date startDate;

    private Date endDate;

    private Boolean returned;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "booker_fk")
    private User booker;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "book_fk")
    private Book book;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Boolean getReturned() {
        return returned;
    }

    public void setReturned(Boolean returned) {
        this.returned = returned;
    }

    public User getBooker() {
        return booker;
    }

    public void setBooker(User booker) {
        this.booker = booker;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}
