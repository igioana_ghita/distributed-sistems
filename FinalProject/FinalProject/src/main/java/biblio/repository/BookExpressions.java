package biblio.repository;

import biblio.model.QBook;
import com.querydsl.core.types.dsl.BooleanExpression;

/**
 * insert comment here
 */
public class BookExpressions {

    public static BooleanExpression priceLessThan(Integer price) {
        QBook book = QBook.book;
        return book.price.lt(price);
    }

    public static BooleanExpression hasYear(Integer year) {
        QBook book = QBook.book;
        return book.year.eq(year);
    }

    public static BooleanExpression likeAuthor(String author) {
        QBook book = QBook.book;
        return book.author.like("%" + author + "%");
    }

    public static BooleanExpression likeTitle(String title) {
        QBook book = QBook.book;
        return book.title.like("%" + title + "%");
    }

    public static BooleanExpression exists() {
        QBook book = QBook.book;
        return book.id.gt(0);
    }

}
