package biblio.repository;

import biblio.model.Membership;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * insert comment here
 */
public interface MembershipRepository extends JpaRepository<Membership, Long> {

    Membership findOneById(Long id);

}