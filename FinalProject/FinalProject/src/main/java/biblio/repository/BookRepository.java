package biblio.repository;

import biblio.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import java.util.List;

/**
 * insert comment here
 */
public interface BookRepository extends JpaRepository<Book, Long>, QueryDslPredicateExecutor<Book> {

    Book findOneById(Long id);

}
