package biblio.repository;

import biblio.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * insert comment here
 */
public interface UserRepository extends JpaRepository<User, Long> {

    User findOneById(Long id);

    User findOneByUsername(String username);

}


