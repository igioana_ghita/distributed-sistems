package biblio.repository;

import biblio.model.Book;
import biblio.model.Reservation;
import biblio.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * insert comment here
 */
public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    List<Reservation> findByBookAndReturned(Book book, Boolean returned);

    List<Reservation> findByBooker(User booker);
}
