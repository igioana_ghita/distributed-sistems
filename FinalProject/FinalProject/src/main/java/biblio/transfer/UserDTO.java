package biblio.transfer;

import biblio.model.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * insert comment here
 */
public class UserDTO implements Serializable {

    private static final int USER_TYPE = 0;
    private static final int ADMIN_TYPE = 1;

    private long id;

    private String username;

    private String email;

    private Date birthDate;

    private String address;

    private String type;

    public UserDTO() {

    }

    public UserDTO(User user) {
        id = user.getId();
        username = user.getUsername();
        email = user.getEmail();
        birthDate = user.getBirthDate();
        address = user.getAddress();
        type = user.getType() == USER_TYPE ? "User" : "Admin";
    }

    public static List<UserDTO> transformFromUserToUserDTOCollection(List<User> users) {
        List<UserDTO> userDTOS = new ArrayList<>();
        for (User user : users) {
            userDTOS.add(new UserDTO(user));
        }
        return userDTOS;
    }

    public static void transformFromUserDTOToUser(UserDTO user, User userEntity) {
        userEntity.setUsername(user.getUsername());
        userEntity.setAddress(user.getAddress());
        userEntity.setBirthDate(user.getBirthDate());
        userEntity.setEmail(user.getEmail());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
