package biblio.transfer;

import biblio.model.Reservation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * insert comment here
 */
public class ReservationDTO implements Serializable {

    private Long id;

    private String username;

    private String book;

    private Date bookDate;

    private Date returnDate;

    private Boolean returned;

    public ReservationDTO() {
    }

    public ReservationDTO(Reservation reservation) {
        id = reservation.getId();
        username = reservation.getBooker().getUsername();
        book = reservation.getBook().getTitle();
        bookDate = reservation.getStartDate();
        returnDate = reservation.getEndDate();
        returned = reservation.getReturned();
    }

    public static List<ReservationDTO> transformFromReservationToReservationDTOCollection(List<Reservation> reservations) {
        List<ReservationDTO> reservationDTOS = new ArrayList<>();
        for (Reservation reservation : reservations) {
            reservationDTOS.add(new ReservationDTO(reservation));
        }
        return reservationDTOS;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBook() {
        return book;
    }

    public void setBook(String book) {
        this.book = book;
    }

    public Date getBookDate() {
        return bookDate;
    }

    public void setBookDate(Date bookDate) {
        this.bookDate = bookDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public Boolean getReturned() {
        return returned;
    }

    public void setReturned(Boolean returned) {
        this.returned = returned;
    }
}
