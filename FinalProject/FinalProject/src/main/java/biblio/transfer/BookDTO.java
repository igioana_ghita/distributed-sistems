package biblio.transfer;

import biblio.model.Book;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * insert comment here
 */
public class BookDTO implements Serializable {

    private long id;

    private String title;

    private Integer year;

    private String author;

    private Integer price;

    private String createdBy;

    private String bookedBy;

    private List<ReservationDTO> reservationDTOS;

    public BookDTO() {
    }

    public BookDTO(Book book) {
        id = book.getId();
        title = book.getTitle();
        year = book.getYear();
        author = book.getAuthor();
        price = book.getPrice();
        createdBy = book.getCreator().getUsername();
        reservationDTOS = ReservationDTO.transformFromReservationToReservationDTOCollection(book.getBookings());
    }

    public static List<BookDTO> transformFromBookToBookDTOCollection(List<Book> books) {
        List<BookDTO> bookDTOS = new ArrayList<>();
        for (Book book : books) {
            bookDTOS.add(new BookDTO(book));
        }
        return bookDTOS;
    }

    public static void transformFromBookDTOToBook(BookDTO bookDTO, Book book) {
        book.setTitle(bookDTO.getTitle());
        book.setAuthor(bookDTO.getAuthor());
        book.setPrice(bookDTO.getPrice());
        book.setYear(bookDTO.getYear());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getBookedBy() {
        return bookedBy;
    }

    public void setBookedBy(String bookedBy) {
        this.bookedBy = bookedBy;
    }

    public List<ReservationDTO> getReservationDTOS() {
        return reservationDTOS;
    }

    public void setReservationDTOS(List<ReservationDTO> reservationDTOS) {
        this.reservationDTOS = reservationDTOS;
    }
}
