package biblio.service;

import biblio.model.User;
import biblio.transfer.ReservationDTO;

import java.util.List;

/**
 * insert comment here
 */
public interface ReservationService {
    List<ReservationDTO> getReservationsForUser(User user);
}
