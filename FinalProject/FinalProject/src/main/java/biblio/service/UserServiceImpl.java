package biblio.service;

import biblio.model.Membership;
import biblio.model.User;
import biblio.repository.MembershipRepository;
import biblio.repository.UserRepository;
import biblio.transfer.RegisterUserDTO;
import biblio.transfer.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * insert comment here
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    MembershipRepository membershipRepository;

    @Autowired
    SecurityService securityService;

    @Override
    public UserDTO getUserById(Long userId) {
        return new UserDTO(userRepository.findOneById(userId));
    }

    @Override
    public Integer loginUser(String username, String password) {
        User user = userRepository.findOneByUsername(username);
        if (user != null) {
            if (securityService.checkLoginForMembership(password, user.getMembership())) {
                return user.getType();
            }
            return -1;
        }
        return -1;
    }

    @Override
    public List<UserDTO> loadAllUsers() {
        userRepository.flush();
        List<User> users = userRepository.findAll();
        List<UserDTO> userDTOS = UserDTO.transformFromUserToUserDTOCollection(users);
        return userDTOS;
    }

    @Override
    public void editUser(UserDTO userDTO) {
        User user = userRepository.findOneById(userDTO.getId());
        if (user == null) {
            return;
        }
        UserDTO.transformFromUserDTOToUser(userDTO, user);
        userRepository.save(user);
    }

    @Override
    public void removeUser(Long id) {
        User user = userRepository.findOneById(id);
        membershipRepository.delete(user.getMembership().getId());
        userRepository.delete(id);
    }

    @Override
    public User getUserByUsername(String username) {
        return userRepository.findOneByUsername(username);
    }

    @Override
    public void createUser(RegisterUserDTO userDTO) {
        User user = userRepository.findOneByUsername(userDTO.getUsername());
        if (user != null) {
            throw new IllegalArgumentException("cannot register user");
        }
        Membership membership = securityService.createMembership(userDTO.getPassword());
        user = new User();
        UserDTO.transformFromUserDTOToUser(userDTO, user);
        user.setMembership(membership);
        userRepository.save(user);
    }
}
