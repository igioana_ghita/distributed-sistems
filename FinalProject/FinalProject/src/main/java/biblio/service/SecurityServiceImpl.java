package biblio.service;

import biblio.model.Membership;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Date;

/**
 * insert comment here
 */
@Service
public class SecurityServiceImpl implements SecurityService {

    @Override
    public Membership createMembership(String password) {
        Membership membership = new Membership();
        MessageDigest md = getMessageDigestAndSetAlgorithm(membership);
        byte[] salt = generateSalt();
        byte[] encryptedPassword = encryptPassword(password, md, salt);
        membership.setEncryptedPassword(encryptedPassword);
        membership.setPassword(password);
        membership.setSalt(salt);
        return membership;
    }

    private byte[] generateSalt() {
        SecureRandom random = new SecureRandom(new Date().toString().getBytes());
        return new BigInteger(130, random).toString(32).getBytes();
    }

    private MessageDigest getMessageDigestAndSetAlgorithm(Membership membership) {
        MessageDigest md = null;
        int algorithm = 0;

        try {
            md = MessageDigest.getInstance("SHA-512");
            algorithm = 1;
        } catch (NoSuchAlgorithmException e) {
            //ignored
        }
        if (algorithm == 0) {
            try {
                md = MessageDigest.getInstance("SHA-256");
                algorithm = 2;
            } catch (NoSuchAlgorithmException e1) {
                // ignored
            }
        }
        membership.setAlgorithm(algorithm);
        return md;
    }

    private byte[] encryptPassword(String password, MessageDigest md, byte[] salt) {
        if (md == null) {
            return password.getBytes();
        }
        md.update(password.getBytes());
        md.update(salt);
        return md.digest();
    }

    @Override
    public boolean checkLoginForMembership(String password, Membership membership) {
        MessageDigest md = getMessageDigestByAlgorithm(membership.getAlgorithm());
        byte[] encryptedPassword = encryptPassword(password, md, membership.getSalt());
        return Arrays.equals(membership.getEncryptedPassword(), encryptedPassword);
    }

    private MessageDigest getMessageDigestByAlgorithm(Integer algorithm) {
        MessageDigest md = null;
        if (algorithm == 0) {
            return null;
        }
        if (algorithm == 1) {
            try {
                md = MessageDigest.getInstance("SHA-512");
            } catch (NoSuchAlgorithmException e) {
                //ignored
            }
        }
        if (algorithm == 2) {
            try {
                md = MessageDigest.getInstance("SHA-256");
            } catch (NoSuchAlgorithmException e) {
                //ignored
            }
        }
        return md;
    }

}
