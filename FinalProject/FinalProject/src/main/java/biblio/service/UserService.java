package biblio.service;

import biblio.model.User;
import biblio.transfer.RegisterUserDTO;
import biblio.transfer.UserDTO;

import java.util.List;

/**
 * insert comment here
 */
public interface UserService {

    UserDTO getUserById(Long userId);

    Integer loginUser(String username, String password);

    List<UserDTO> loadAllUsers();

    void editUser(UserDTO user);

    void removeUser(Long id);

    User getUserByUsername(String username);

    void createUser(RegisterUserDTO user);
}
