package biblio.service;

import biblio.model.Membership;

/**
 * insert comment here
 */
public interface SecurityService {

    Membership createMembership(String password);

    boolean checkLoginForMembership(String password, Membership membership);
}
