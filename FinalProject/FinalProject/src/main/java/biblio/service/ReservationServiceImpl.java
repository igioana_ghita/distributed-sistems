package biblio.service;

import biblio.model.Reservation;
import biblio.model.User;
import biblio.repository.ReservationRepository;
import biblio.transfer.ReservationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * insert comment here
 */
@Service
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    ReservationRepository reservationRepository;

    @Override
    public List<ReservationDTO> getReservationsForUser(User user) {
        List<Reservation> reservations = reservationRepository.findByBooker(user);
        return ReservationDTO.transformFromReservationToReservationDTOCollection(reservations);
    }
}
