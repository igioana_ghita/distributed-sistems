package biblio.service;

import biblio.model.Book;
import biblio.model.Reservation;
import biblio.model.User;
import biblio.repository.BookExpressions;
import biblio.repository.BookRepository;
import biblio.repository.ReservationRepository;
import biblio.transfer.BookDTO;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * insert comment here
 */
@Service
public class BookServiceImpl implements BookService {

    @Autowired
    BookRepository bookRepository;

    @Autowired
    ReservationRepository reservationRepository;

    @Override
    public List<BookDTO> loadAllBooks(BookDTO filter) {
        bookRepository.flush();
        List<Book> books;
        BooleanExpression expression = BookExpressions.exists();
        if (filter.getAuthor() != null && !filter.getAuthor().isEmpty()) {
            expression = expression.and(BookExpressions.likeAuthor(filter.getAuthor()));
        }
        if (filter.getTitle() != null && !filter.getTitle().isEmpty()) {
            expression = expression.and(BookExpressions.likeTitle(filter.getTitle()));
        }
        if (filter.getPrice() != null) {
            expression = expression.and(BookExpressions.priceLessThan(filter.getPrice()));
        }
        if (filter.getYear() != null) {
            expression = expression.and(BookExpressions.hasYear(filter.getYear()));
        }
        books = new ArrayList<>((Collection<? extends Book>) bookRepository.findAll(expression));

        List<BookDTO> bookDTOS = new ArrayList<>();
        for (Book book : books) {
            BookDTO bookDTO = new BookDTO(book);
            List<Reservation> reservations = reservationRepository.findByBookAndReturned(book, false);
            for (Reservation reservation : reservations) {
                bookDTO.setBookedBy(reservation.getBooker().getUsername());
            }
            bookDTOS.add(bookDTO);
        }
        return bookDTOS;
    }

    @Override
    public void insertBook(BookDTO bookDTO, User user) {
        Book book = new Book();
        BookDTO.transformFromBookDTOToBook(bookDTO, book);
        book.setCreator(user);
        bookRepository.saveAndFlush(book);
    }

    @Override
    public void editBook(BookDTO bookDTO) {
        Book book = bookRepository.findOneById(bookDTO.getId());
        if (book == null) {
            return;
        }
        BookDTO.transformFromBookDTOToBook(bookDTO, book);
        bookRepository.save(book);
    }

    @Override
    public BookDTO getBook(Long id) {
        return new BookDTO(bookRepository.findOneById(id));
    }

    @Override
    public void removeBook(Long id) {
        Book book = bookRepository.findOneById(id);
        reservationRepository.delete(book.getBookings());
        bookRepository.delete(id);
    }

    @Override
    public void bookBook(Long id, User user) {
        Reservation reservation = new Reservation();
        reservation.setStartDate(new Date());
        reservation.setBook(bookRepository.findOneById(id));
        reservation.setBooker(user);
        reservation.setReturned(false);
        reservationRepository.save(reservation);
    }

    @Override
    public void returnBook(Long id) {
        Book book = bookRepository.findOneById(id);
        List<Reservation> reservations = reservationRepository.findByBookAndReturned(book, false);
        for (Reservation reservation : reservations) {
            reservation.setReturned(true);
            reservation.setEndDate(new Date());
            reservationRepository.save(reservation);
        }
    }

    @Override
    public void exportCsv(BookDTO filter) {
        try {
            File file = new File ("src/main/resources/export.csv" );
            PrintWriter printWriter = new PrintWriter(file);
            List<BookDTO> bookDTOS = this.loadAllBooks(filter);
            printWriter.println("id,title,year,author,price,createdBy,booked");
            for(BookDTO bookDTO : bookDTOS){
                printWriter.print(bookDTO.getId()+",");
                printWriter.print(bookDTO.getTitle()+",");
                printWriter.print(bookDTO.getYear()+",");
                printWriter.print(bookDTO.getAuthor()+",");
                printWriter.print(bookDTO.getPrice()+",");
                printWriter.print(bookDTO.getCreatedBy()+",");
                printWriter.println(String.valueOf(bookDTO.getBookedBy() != null && !bookDTO.getBookedBy().isEmpty()));
            }
            printWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
