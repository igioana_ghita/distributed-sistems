package biblio.service;

import biblio.model.User;
import biblio.transfer.BookDTO;

import java.util.List;

/**
 * insert comment here
 */
public interface BookService {

    List<BookDTO> loadAllBooks(BookDTO filter);

    void insertBook(BookDTO bookDTO, User user);

    void editBook(BookDTO bookDTO);

    BookDTO getBook(Long id);

    void removeBook(Long id);

    void bookBook(Long id, User user);

    void returnBook(Long id);

    void exportCsv(BookDTO filter);
}
