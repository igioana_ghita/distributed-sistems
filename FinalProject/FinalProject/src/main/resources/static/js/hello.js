angular.module('hello', [ 'ngRoute' ]).config(function($routeProvider, $httpProvider) {

	$routeProvider.when('/', {
		templateUrl : 'home.html',
		controller : 'home',
		controllerAs: 'controller'
	}).when('/login', {
		templateUrl : 'login.html',
		controller : 'navigation',
		controllerAs: 'controller'
	}).when('/register', {
      	    templateUrl : 'register.html',
      	    controller : 'register',
      	    controllerAs : 'controller'
    }).when('/allUsers', {
	    templateUrl : 'allUsers.html',
	    controller : 'allUsers',
	    controllerAs : 'controller'
	}).when('/allBooks', {
      	    templateUrl : 'allBooks.html',
      	    controller : 'allBooks',
      	    controllerAs : 'controller'
    }).when('/editUser', {
      	    templateUrl : 'editUser.html',
      	    controller : 'editUser',
      	    controllerAs : 'controller'
    }).when('/insertBook', {
      	    templateUrl : 'insertBook.html',
      	    controller : 'insertBook',
      	    controllerAs : 'controller'
    }).when('/editBook', {
      	    templateUrl : 'editBook.html',
      	    controller : 'editBook',
      	    controllerAs : 'controller'
    }).otherwise('/');

	$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';









        }).controller('navigation', function($rootScope, $http, $location, $route) {

            $rootScope.error = false;
			var self = this;

			self.tab = function(route) {
				return $route.current && route === $route.current.controller;
			};

			var authenticate = function(credentials, callback) {

				var headers = credentials ? {
					authorization : "Basic "
							+ btoa(credentials.username + ":"
									+ credentials.password)
				} : {};

				$http.get('user', {
					headers : headers
				}).then(function(response) {
                    $rootScope.admin = false;
					if (response.data.name) {
						$rootScope.authenticated = true;
						angular.forEach(response.data.authorities, function(value, key) {
						    if(value.authority == "ROLE_ADMIN"){
						        $rootScope.admin = true;
						    }
						});
					} else {
						$rootScope.authenticated = false;
					}
					callback && callback($rootScope.authenticated);
				}, function() {
					$rootScope.authenticated = false;
					callback && callback(false);
				});

			}

			authenticate();

			self.credentials = {};
			self.login = function() {
				authenticate(self.credentials, function(authenticated) {
					if (authenticated) {
						console.log("Login succeeded")
						$location.path("/");
						$rootScope.error = false;
						$rootScope.authenticated = true;
					} else {
						console.log("Login failed")
						$location.path("/login");
						$rootScope.error = true;
						$rootScope.authenticated = false;
					}
				})
			};

			self.logout = function() {
				$http.post('logout', {}).finally(function() {
					$rootScope.authenticated = false;
					$location.path("/");
				});
			}

			self.usersAdmin = function() {
            	$location.path("/allUsers");
            }

            self.listBooks = function() {
            	$location.path("/allBooks");
            }

            self.register = function() {
            	$location.path("/register");
            }









        }).controller('register', function($rootScope, $http, $location) {

            $rootScope.error = false;
            $rootScope.error2 = false;
            $rootScope.success = false;
			var self = this;

		    self.credentials = {};

			self.register = function() {
			    if(self.credentials.password == self.credentials.rePassword){
                    var data = self.credentials;
                    $http.post('/users/register',data).then(function(response) {
                        $rootScope.error = false;
                        $rootScope.error2 = false;
                        $rootScope.success = true;
                        self.credentials = {};
                    }, function(){
                        $rootScope.error = true;
                        $rootScope.error2 = false;
                        $rootScope.success = false;
                    });
				} else {
				    $rootScope.error2 = true;
				}

            };








		}).controller('home', function($scope, $http) {
            $scope.columns =[
                {
                    "title": "username",
                    "type": "string"
                },
                {
                    "title": "book",
                    "type": "string"
                },
                {
                    "title": "bookDate",
                    "type": "date"
                },
                {
                    "title": "returnDate",
                    "type": "date"
                },
                {
                    "title": "returned",
                    "type": "boolean"
                }];
            $http.get('/reservations/getForUser').then(function(response) {

                angular.forEach(response.data, function(value, key) {
                    if (value.bookDate){
                        value.bookDate = new Date(value.bookDate);
                    }
                    if (value.returnDate){
                        value.returnDate = new Date(value.returnDate);
                    }
                });
                $scope.data = response.data;
            })










        }).controller('allUsers', function($rootScope, $scope, $http, $location) {
            var self = this;
            $scope.columns =[
                {
                    "title": "id",
                    "type": "number"
                },
                {
                    "title": "username",
                    "type": "string"
                },
                {
                    "title": "email",
                    "type": "string"
                },
                {
                    "title": "birthDate",
                    "type": "string"
                },
                {
                    "title": "address",
                    "type": "string"
                },
                {
                    "title": "type",
                    "type": "string"
                }];
            var refresh = function(){
                $http.get('/users/getAll').then(function(response) {

                            angular.forEach(response.data, function(value, key) {
                                if(value.birthDate){
                                    value.birthDate = new Date(value.birthDate);
                                }
                            });
                            $scope.data = response.data;
                        })

            };
            refresh();

            $scope.edit = function(idx) {
                var user = $scope.data[idx];
                $rootScope.editUser = user.id;
                $location.path('/editUser');
            };
            $scope.remove = function(idx) {
                var user = $scope.data[idx];
                var data = {"id": user.id};
                $http.post('/users/removeUser',data).then(function(response) {
                    self.user = response.data;
                    refresh();
                });

            };









        }).controller('editUser', function($rootScope, $scope, $http, $location){
            $rootScope.error = false;
            var userId = $rootScope.editUser;
            var self = this;
            var data = {"id": userId};
            $http.post('/users/getUser',data).then(function(response) {
                self.user = response.data;
            });

            self.edit = function(){
                $rootScope.error = false;
                var data = self.user;
                $http.post('/users/editUser',data).then(function(response) {
            	    $location.path("/allUsers");
                }, function() {
					$rootScope.error = true;
                    callback && callback(false);
                });

            }








        }).controller('allBooks', function($rootScope, $scope, $http, $location) {
            $rootScope.invalidBooking = false;
            var self = this;
            $scope.columns =[
            {
                "title": "title",
                "type": "string"
            },
            {
                "title": "year",
                "type": "number"
            },
            {
                "title": "author",
                "type": "string"
            },
            {
                "title": "price",
                "type": "number"
            },
            {
                "title": "createdBy",
                "type": "string"
            },
            {
                "title": "bookedBy",
                "type": "string"
            }];
            self.filterObject = {};
            $scope.filterObject = {};
            $scope.data = {};
            var refresh = function(){
                $http.post('/books/getAll', self.filterObject).then(function(response) {
                    $scope.data = response.data;
                })
            };
            refresh();

            $scope.edit = function(idx) {
                var book = $scope.data[idx];
                $rootScope.editBook = book.id;
                $location.path('/editBook');
            };

            $scope.remove = function(idx) {
                var book = $scope.data[idx];
                if (book.bookedBy == null){
                    $rootScope.invalidBooking = false;
                    var data = {"id": book.id};
                    $http.post('/books/removeBook', data).then(function(response) {
                        refresh();
                    });
                } else {
                    $rootScope.invalidBooking = true;
                }
            };

            $scope.book = function(idx) {
                var book = $scope.data[idx];
                if (book.bookedBy == null){
                    $rootScope.invalidBooking = false;
                    var data = {"id": book.id};
                    $http.post('/books/bookBook', data).then(function(response) {
                        refresh();
                    });
                } else {
                    $rootScope.invalidBooking = true;
                }
            };

            $scope.returned = function(idx) {
                $rootScope.invalidBooking = false;
                var book = $scope.data[idx];
                if (book.bookedBy == null){
                      $rootScope.invalidBooking = true;
                } else {
                    var data = {"id": book.id};
                    $http.post('/books/returnBook', data).then(function(response) {
                        refresh();
                    });
                }
            };

            $scope.insert = function() {
                $location.path("/insertBook");
            };

            $scope.filterGrid = function(){
                self.filterObject = {
                    "title" : document.getElementsByName("title")[0].value,
                    "year" : document.getElementsByName("year")[0].value,
                    "author" : document.getElementsByName("author")[0].value,
                    "price" : document.getElementsByName("price")[0].value,
                };
                refresh();
            }

            $scope.clearFilter = function(){
                document.getElementsByName("title")[0].value = "";
                document.getElementsByName("year")[0].value = "";
                document.getElementsByName("author")[0].value = "";
                document.getElementsByName("price")[0].value = "";
                self.filterObject = {};
                refresh();
            }

            $scope.exportAsCsv = function(){
                $http.post('/download', self.filterObject).then(function(response) {
                     var anchor = angular.element('<a/>');
                     anchor.attr({
                         href: 'data:attachment/csv;charset=utf-8,' + encodeURI(response.data),
                         target: '_blank',
                         download: 'filename.csv'
                     })[0].click();

                  });
            }







        }).controller('insertBook', function($rootScope, $scope, $http, $location){
            $rootScope.error = false;
            var self = this;
            self.book = {};

            self.insert = function(){
                var data = self.book;
                $http.post('/books/insertBook', data).then(function(response) {
            	    $location.path("/allBooks");
                }, function() {
					$rootScope.error = true;
                    callback && callback(false);
                });
            }








        }).controller('editBook', function($rootScope, $scope, $http, $location){
            $scope.columns =[
                {
                    "title": "username",
                    "type": "string"
                },
                {
                    "title": "book",
                    "type": "string"
                },
                {
                    "title": "bookDate",
                    "type": "date"
                },
                {
                    "title": "returnDate",
                    "type": "date"
                },
                {
                    "title": "returned",
                    "type": "boolean"
                }];
            $rootScope.error = false;
            var bookId = $rootScope.editBook;
            var self = this;
            var data = {"id": bookId};
            $http.post('/books/getBook',data).then(function(response) {
                angular.forEach(response.data.reservationDTOS, function(value, key) {
                    if (value.bookDate){
                        value.bookDate = new Date(value.bookDate);
                    }
                    if (value.returnDate){
                        value.returnDate = new Date(value.returnDate);
                    }
                });

                $scope.data = response.data.reservationDTOS;
                self.book = response.data;

            });


            self.edit = function(){
                $rootScope.error = false;
                var data = self.book;
                $http.post('/books/editBook', data).then(function(response) {
            	    $location.path("/allBooks");
                }, function() {
					$rootScope.error = true;
                    callback && callback(false);
                });

            }


        });

