package ro.tuc.dsrl.ds.handson.assig.one.client.views;

import ro.tuc.dsrl.ds.handson.assig.one.client.entities.Student;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 * Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description: CatalogView is a JFrame which contains the UI elements of the Client application.
 */
public class CatalogView extends JFrame {

    private static final long serialVersionUID = 1L;
    private JTextField textFirstName;
    private JTextField textLastName;
    private JTextField textMail;
    private JTextField textSearchId;
    private JTextField textRemoveId;
    private JButton btnRemove;
    private JButton btnGet;
    private JButton btnPost;
    private JTextArea textArea;

    public CatalogView() {
        setTitle("HTTP Protocol simulator");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(300, 100, 450, 300);
        setResizable(false);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblInsertNewStudent = new JLabel("Insert new student");
        lblInsertNewStudent.setBounds(10, 11, 140, 14);
        contentPane.add(lblInsertNewStudent);

        JLabel lblFirstName = new JLabel("First name");
        lblFirstName.setBounds(10, 36, 100, 14);
        contentPane.add(lblFirstName);

        JLabel lblLastName = new JLabel("Last name");
        lblLastName.setBounds(10, 61, 100, 14);
        contentPane.add(lblLastName);

        JLabel lblMail = new JLabel("Mail");
        lblMail.setBounds(10, 86, 46, 14);
        contentPane.add(lblMail);

        textFirstName = new JTextField();
        textFirstName.setBounds(100, 33, 86, 20);
        contentPane.add(textFirstName);
        textFirstName.setColumns(10);

        textLastName = new JTextField();
        textLastName.setBounds(100, 58, 86, 20);
        contentPane.add(textLastName);
        textLastName.setColumns(10);

        textMail = new JTextField();
        textMail.setBounds(100, 83, 86, 20);
        contentPane.add(textMail);
        textMail.setColumns(10);

        btnPost = new JButton("POST");
        btnPost.setBounds(10, 132, 89, 23);
        contentPane.add(btnPost);

        JLabel lblRemoveStudentById = new JLabel("Delete student by id");
        lblRemoveStudentById.setBounds(10, 160, 150, 14);
        contentPane.add(lblRemoveStudentById);

        JLabel lblRemoveId = new JLabel("Id");
        lblRemoveId.setBounds(10, 176, 46, 14);
        contentPane.add(lblRemoveId);

        textRemoveId = new JTextField();
        textRemoveId.setBounds(70, 176, 86, 20);
        contentPane.add(textRemoveId);
        textRemoveId.setColumns(10);

        btnRemove = new JButton("REMOVE");
        btnRemove.setBounds(10, 200, 100, 23);
        contentPane.add(btnRemove);

        JLabel lblFindStudentById = new JLabel("Find student by id");
        lblFindStudentById.setBounds(235, 11, 145, 14);
        contentPane.add(lblFindStudentById);

        JLabel lblSearchId = new JLabel("Id");
        lblSearchId.setBounds(235, 36, 46, 14);
        contentPane.add(lblSearchId);

        textSearchId = new JTextField();
        textSearchId.setBounds(320, 33, 86, 20);
        contentPane.add(textSearchId);
        textSearchId.setColumns(10);

        btnGet = new JButton("GET");
        btnGet.setBounds(235, 77, 89, 23);
        contentPane.add(btnGet);

        textArea = new JTextArea();
        textArea.setBounds(235, 131, 171, 120);
        contentPane.add(textArea);
    }

    public void addBtnGetActionListener(ActionListener e) {
        btnGet.addActionListener(e);
    }

    public void addBtnPostActionListener(ActionListener e) {
        btnPost.addActionListener(e);
    }

    public void addBtnRemoveActionListener(ActionListener e) {
        btnRemove.addActionListener(e);
    }

    public String getStudentSearchId() {
        return textSearchId.getText();
    }

    public String getStudentRemoveId() {
        return textRemoveId.getText();
    }

    public String getFirstName() {
        return textFirstName.getText();
    }

    public String getLastName() {
        return textLastName.getText();
    }

    public String getMail() {
        return textMail.getText();
    }

    public void printStudent(Student student) {
        textArea.setText(student.toString());
    }

    public void clear() {
        textSearchId.setText("");
        textRemoveId.setText("");
        textFirstName.setText("");
        textLastName.setText("");
        textMail.setText("");
    }
}
