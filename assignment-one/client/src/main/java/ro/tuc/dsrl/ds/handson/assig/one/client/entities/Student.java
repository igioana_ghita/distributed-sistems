package ro.tuc.dsrl.ds.handson.assig.one.client.entities;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 * Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-server
 * @Since: Aug 28, 2015
 * @Description: Class describing a Student.
 */
public class Student {

    private int id;
    private String firstName;
    private String lastName;
    private String mail;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Override
    public String toString() {
        return "Student\n id: " + id + "\n First name: " + firstName + "\n Last name: " + lastName + "\n Mail: " + mail;
    }

}
