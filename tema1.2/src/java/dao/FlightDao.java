package dao;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;

import java.util.List;
import model.Flight;
import org.hibernate.cfg.Configuration;

public class FlightDao {

    private static final Log LOGGER = LogFactory.getLog(FlightDao.class);

    private static FlightDao INSTANCE = new FlightDao();

    private FlightDao() {
        if (INSTANCE != null) {
            throw new IllegalStateException("Already instantiated");
        }
    }

    public static FlightDao getInstance() {
        return INSTANCE;
    }
    private final SessionFactory factory = new Configuration().configure().buildSessionFactory();

    @SuppressWarnings("unchecked")
    public Flight findFlight(Long id) {
        Session session = factory.openSession();
        Transaction tx = null;
        Flight flight = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight WHERE id = :id");
            query.setParameter("id", id);
            flight = (Flight) query.uniqueResult();
            if (flight != null) {
                Hibernate.initialize(flight.getArrivalCity());
                Hibernate.initialize(flight.getDepartureCity());
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return flight;
    }

    public List<Flight> listFlights() {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight");
            flights = query.list();
            for (Flight flight : flights) {
                Hibernate.initialize(flight.getArrivalCity());
                Hibernate.initialize(flight.getDepartureCity());
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return flights;
    }

    public Flight update(Flight flight) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return flight;
    }

    public Flight create(Flight flight) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return flight;
    }

    public void remove(Flight flight) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
    }
}
