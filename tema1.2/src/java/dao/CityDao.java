package dao;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;

import model.City;
import org.hibernate.cfg.Configuration;

public class CityDao {

    private static final Log LOGGER = LogFactory.getLog(CityDao.class);

    private static CityDao INSTANCE = new CityDao();

    private CityDao() {
        if (INSTANCE != null) {
            throw new IllegalStateException("Already instantiated");
        }
    }

    public static CityDao getInstance() {
        return INSTANCE;
    }
    private final SessionFactory factory = new Configuration().configure().buildSessionFactory();

    @SuppressWarnings("unchecked")
    public City findCity(Long id) {
        Session session = factory.openSession();
        Transaction tx = null;
        City city = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM City WHERE id = :id");
            query.setParameter("id", id);
            city = (City) query.uniqueResult();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return city;
    }

    public City findCity(String name) {
        Session session = factory.openSession();
        Transaction tx = null;
        City city = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM City WHERE name = :name");
            query.setParameter("name", name);
            city = (City) query.uniqueResult();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return city;
    }

}
