package servlet;

import dao.CityDao;
import dao.FlightDao;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.City;
import model.Flight;
import model.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import services.CityService;

public class GetLocalTimeServlet extends BaseServlet {

    private static final Log LOGGER = LogFactory.getLog(GetLocalTimeServlet.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOGGER.info("Get local time servlet called");
        handleProcessRequest(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    @Override
    protected void processCorrectRequest(User user, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Flight> flights = FlightDao.getInstance().listFlights();
        request.setAttribute("flights", flights);
        
        String cityString = request.getParameter("city");
        if (cityString == null || cityString.trim().equals("")) {
            request.setAttribute("error", "Invalid city");
            RequestDispatcher view = request.getRequestDispatcher("flightsClient.jsp");
            view.forward(request, response);
            return;
        }
        City city = CityDao.getInstance().findCity(cityString);
        if (city == null) {
            request.setAttribute("error", "Invalid city");
            RequestDispatcher view = request.getRequestDispatcher("flightsClient.jsp");
            view.forward(request, response);
            return;
        }
        
        String localTime = CityService.getInstance().getLocalTime(city);
        
        request.setAttribute("localTime", localTime == null ? "Wrong coordinates!" : localTime);
        request.setAttribute("requestedCity", city.getName());

        RequestDispatcher view = request.getRequestDispatcher("flightsClient.jsp");
        view.forward(request, response);
    }

    @Override
    protected List<Long> permittedRoles() {
        return Arrays.asList(2l);
    }

}
