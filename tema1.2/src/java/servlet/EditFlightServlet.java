package servlet;

import dao.CityDao;
import dao.FlightDao;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.City;
import model.Flight;
import model.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class EditFlightServlet extends BaseServlet {

    private static final Log LOGGER = LogFactory.getLog(EditFlightServlet.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOGGER.info("Edit Flight servlet called");
        handleProcessRequest(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    @Override
    protected void processCorrectRequest(User user, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        request.setAttribute("error", null);

        if (action.equals("Update")) {
            LOGGER.info("update");
            updateFlight(request, response);
        }
        if (action.equals("Insert")) {
            LOGGER.info("insert");
            insertFlight(request, response);
        }
        if (action.equals("Remove")) {
            LOGGER.info("remove");
            removeFlight(request, response);
        }
        List<Flight> flights = FlightDao.getInstance().listFlights();
        request.setAttribute("flights", flights);

        RequestDispatcher view = request.getRequestDispatcher("flightsAdmin.jsp");
        view.forward(request, response);
    }

    private void updateFlight(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String flightString = request.getParameter("flightNumber");
        if (flightString == null || flightString.trim().equals("")) {
            request.setAttribute("error", "Invalid flight number");
            return;
        }
        Long flightId = Long.valueOf(flightString);
        Flight flight = FlightDao.getInstance().findFlight(flightId);
        if (flight == null) {
            request.setAttribute("error", "Invalid flight number");
            return;
        }
        String airplaneType = request.getParameter("airplaneType");
        if (airplaneType != null && !airplaneType.trim().equals("")) {
            flight.setAirplaneType(airplaneType);
        }
        String departureCity = request.getParameter("departureCity");
        if (departureCity != null && !departureCity.trim().equals("")) {
            City city = CityDao.getInstance().findCity(departureCity);
            if (city == null) {
                request.setAttribute("error", "Invalid departure city");
                return;
            }
            flight.setDepartureCity(city);
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        String departureDate = request.getParameter("departureDate");
        if (departureDate != null && !departureDate.trim().equals("")) {
            try {
                Date newDeparture = format.parse(departureDate);
                flight.setDepartureDateHour(newDeparture);
            } catch (ParseException ignored) {
                request.setAttribute("error", "Invalid departure date time");
                return;
            }
        }

        String arrivalCity = request.getParameter("arrivalCity");
        if (arrivalCity != null && !arrivalCity.trim().equals("")) {
            City city = CityDao.getInstance().findCity(arrivalCity);
            if (city == null) {
                request.setAttribute("error", "Invalid arrival city");
                return;
            }
            flight.setArrivalCity(city);
        }
        String arrivalDate = request.getParameter("arrivalDate");
        if (arrivalDate != null && !arrivalDate.trim().equals("")) {
            try {
                Date newArrival = format.parse(arrivalDate);
                flight.setArrivalDateHour(newArrival);
            } catch (ParseException ignored) {
                request.setAttribute("error", "Invalid arrival date time");
                return;
            }
        }

        FlightDao.getInstance().update(flight);
    }

    private void insertFlight(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Flight flight = new Flight();

        String airplaneType = request.getParameter("airplaneType");
        if (airplaneType == null || airplaneType.trim().equals("")) {
            request.setAttribute("error", "Invalid airplane type");
            return;
        }
        flight.setAirplaneType(airplaneType);

        String departureCity = request.getParameter("departureCity");
        if (departureCity == null || departureCity.trim().equals("")) {
            request.setAttribute("error", "Invalid departure city");
            return;
        }
        City city = CityDao.getInstance().findCity(departureCity);
        if (city == null) {
            request.setAttribute("error", "Invalid departure city");
            return;
        }
        flight.setDepartureCity(city);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        String departureDate = request.getParameter("departureDate");
        if (departureDate == null || departureDate.trim().equals("")) {
            request.setAttribute("error", "Invalid departure date time");
            return;
        }
        try {
            Date newDeparture = format.parse(departureDate);
            flight.setDepartureDateHour(newDeparture);
        } catch (ParseException ignored) {
            request.setAttribute("error", "Invalid departure date time");
            return;
        }

        String arrivalCity = request.getParameter("arrivalCity");
        if (arrivalCity == null || arrivalCity.trim().equals("")) {
            request.setAttribute("error", "Invalid arrival city");
            return;
        }
        city = CityDao.getInstance().findCity(arrivalCity);
        if (city == null) {
            request.setAttribute("error", "Invalid arrival city");
            return;
        }
        flight.setArrivalCity(city);

        String arrivalDate = request.getParameter("arrivalDate");
        if (arrivalDate == null || arrivalDate.trim().equals("")) {
            request.setAttribute("error", "Invalid arrival date time");
            return;
        }
        try {
            Date newArrival = format.parse(arrivalDate);
            flight.setArrivalDateHour(newArrival);
        } catch (ParseException ignored) {
            request.setAttribute("error", "Invalid arrival date time");
            return;
        }

        FlightDao.getInstance().create(flight);
    }

    private void removeFlight(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String flightString = request.getParameter("flightNumber");
        if (flightString == null || flightString.trim().equals("")) {
            request.setAttribute("error", "Invalid flight number");
            return;
        }
        Long flightId = Long.valueOf(flightString);
        Flight flight = FlightDao.getInstance().findFlight(flightId);
        if (flight == null) {
            request.setAttribute("error", "Invalid flight number");
            return;
        }

        FlightDao.getInstance().remove(flight);
    }
    
    @Override
    protected List<Long> permittedRoles() {
        return Arrays.asList(1l);
    }
}
