package services;

import model.City;

public class CityService {

    private static final CityService INSTANCE = new CityService();

    private CityService() {
        if (INSTANCE != null) {
            throw new IllegalStateException("Already instantiated");
        }
    }

    public static CityService getInstance() {
        return INSTANCE;
    }

    private static final WebServiceCaller webServiceCaller = new WebServiceCaller();

    public String getLocalTime(City city) {
        return webServiceCaller.accessWebService(city.getLatitude(), city.getLongitude());
    }
}
