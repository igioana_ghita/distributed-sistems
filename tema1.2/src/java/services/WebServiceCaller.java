package services;

import java.io.IOException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

public class WebServiceCaller {

    Document xmldoc;

    private static final Log LOGGER = LogFactory.getLog(WebServiceCaller.class);

    public WebServiceCaller() {
    }

    public String accessWebService(String latitude, String longitude) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            LOGGER.info("calling the next url: " + "http://new.earthtools.org/timezone/" + latitude + "/" + longitude);
            xmldoc = factory.newDocumentBuilder().parse("http://new.earthtools.org/timezone/" + latitude + "/" + longitude);
            NodeList node = xmldoc.getDocumentElement().getChildNodes();
            for (int i = 0; i < node.getLength(); i++) {
                Node firstNode = node.item(i);
                if (firstNode.getNodeName().equals("localtime")) {
                    return firstNode.getFirstChild().getNodeValue();
                }
            }
            return null;
        } catch (IOException | ParserConfigurationException | DOMException | SAXException e) {
            e.printStackTrace();
            return null;
        }
    }

}
