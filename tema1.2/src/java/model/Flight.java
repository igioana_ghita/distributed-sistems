package model;

import java.util.Date;

public class Flight {
    
    private Long id;
    private String airplaneType;
    private City departureCity;    
    private Date departureDateHour;
    private City arrivalCity;
    private Date arrivalDateHour;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneType) {
        this.airplaneType = airplaneType;
    }

    public City getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(City departureCity) {
        this.departureCity = departureCity;
    }

    public City getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(City arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public Date getDepartureDateHour() {
        return departureDateHour;
    }

    public void setDepartureDateHour(Date departureDateHour) {
        this.departureDateHour = departureDateHour;
    }

    public Date getArrivalDateHour() {
        return arrivalDateHour;
    }

    public void setArrivalDateHour(Date arrivalDateHour) {
        this.arrivalDateHour = arrivalDateHour;
    }

    @Override
    public String toString() {
        return "Flight{" + "id=" + id + ", airplaneType=" + airplaneType + ", departureCity=" + departureCity.getName() + ", departureDateHour=" + departureDateHour + ", arrivalCity=" + arrivalCity.getName() + ", arrivalDateHour=" + arrivalDateHour + '}';
    }

    
}
