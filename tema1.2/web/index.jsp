<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>WebApp 1</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>

        <h1 align="center">Login Page</h1>
        <form name="Login Form" action="LoginServlet" method="POST">
            Username:<br/>
            <input type="text" name="username" value="" size="20" />
            <br/>
            Password:<br/>
            <input type="password" name="password" value="" size="20" />
            <br/>
            <center>
                <input type="SUBMIT">
            </center>
        </form>

        <%
            String errors = (String) request.getAttribute("loginError");
            if (errors != null) {
                out.println("<br>");
                out.println("<p>");
                out.println(errors);
                out.println("<p/>");
            }

            String loggedOut = (String) request.getAttribute("loggedOut");
            if (loggedOut != null) {
                out.println("<br>");
                out.println("<p>");
                out.println(loggedOut);
                out.println("<p/>");
            }
        %>
    </body>
</html>
