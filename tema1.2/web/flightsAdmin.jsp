<%@page import="model.Flight"%>
<%@ page import="java.util.*" %>

<html><body>
        <h1 align="center">Flights JSP</h1>
        <p>
            <%

                out.println("<br>Welcome to the administrator page<br>");
                List<Flight> flights = (List<Flight>) request.getAttribute("flights");
                if (flights != null && !flights.isEmpty()) {
                    out.println("<table>");
                    out.println("<tr>");
                    out.println("<th>Flight Number</th>");
                    out.println("<th>Airplane Type</th>");
                    out.println("<th>Departure City</th>");
                    out.println("<th>Departure Date Time</th>");
                    out.println("<th>Arrival City</th>");
                    out.println("<th>Arrival Date Time</th>");
                    out.println("</tr>");
                    Iterator<Flight> i = flights.iterator();
                    while (i.hasNext()) {
                        Flight f = i.next();
                        out.println("<tr>");
                        out.println("<td>" + f.getId() + "</td>");
                        out.println("<td>" + f.getAirplaneType() + "</td>");
                        out.println("<td>" + f.getDepartureCity().getName() + "</td>");
                        out.println("<td>" + f.getDepartureDateHour() + "</td>");
                        out.println("<td>" + f.getArrivalCity().getName() + "</td>");
                        out.println("<td>" + f.getArrivalDateHour() + "</td>");
                        out.println("</tr>");
                    }
                    out.println("</table>");
                }
                String error = (String) request.getAttribute("error");
                if (error != null) {
                    out.println("<br>");
                    out.println("<p>");
                    out.println(error);
                    out.println("<p/>");
                }
            %>
        <h3 align="left">Edit Flight</h3>
        <form name="Edit Flight" action="EditFlightServlet" method="POST">
            Flight Number for which the modifications are made<br/>
            <input type="text" name="flightNumber" value="" size="20" />
            <br/>
            New Airplane Type:<br/>
            <input type="text" name="airplaneType" value="" size="20" />
            <br/>
            New Departure City:<br/>
            <input type="text" name="departureCity" value="" size="20" />
            <br/>
            New Departure Date Time:<br/>
            <input type="text" name="departureDate" value="" size="20" />
            <br/>
            New Arrival City:<br/>
            <input type="text" name="arrivalCity" value="" size="20" />
            <br/>            
            New Arrival Date Time:<br/>
            <input type="text" name="arrivalDate" value="" size="20" />
            <br/>            
            <br/>    
            <input type="submit" value="Update" name="action" value="Update" />
            <input type="submit" value="Insert" name="action" value="Insert" />
            <input type="submit" value="Remove" name="action" value="Remove" />
        </form>
        <form name="List Flights" action="ListFlightsAdminServlet" method="POST">
            <input type="submit" value="List Flights" name="List Flights Button" />
        </form>
        <form name="Log out button" action="LogoutServlet" method="POST">
            <input type="submit" value="Log Out" name="Log Out Button" />
        </form>
    </body></html>