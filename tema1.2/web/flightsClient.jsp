<%@page import="model.Flight"%>
<%@ page import="java.util.*" %>

<html><body>
        <h1 align="center">Flights JSP</h1>
        <p>
            <%

                out.println("<br>Welcome to the client page<br>");
                List<Flight> flights = (List<Flight>) request.getAttribute("flights");
                if (flights != null && !flights.isEmpty()) {
                    out.println("<table>");
                    out.println("<tr>");
                    out.println("<th>Flight Number</th>");
                    out.println("<th>Airplane Type</th>");
                    out.println("<th>Departure City</th>");
                    out.println("<th>Departure Date Time</th>");
                    out.println("<th>Arrival City</th>");
                    out.println("<th>Arrival Date Time</th>");
                    out.println("</tr>");
                    Iterator<Flight> i = flights.iterator();
                    while (i.hasNext()) {
                        Flight f = i.next();
                        out.println("<tr>");
                        out.println("<td>" + f.getId() + "</td>");
                        out.println("<td>" + f.getAirplaneType() + "</td>");
                        out.println("<td>" + f.getDepartureCity().getName() + "</td>");
                        out.println("<td>" + f.getDepartureDateHour() + "</td>");
                        out.println("<td>" + f.getArrivalCity().getName() + "</td>");
                        out.println("<td>" + f.getArrivalDateHour() + "</td>");
                        out.println("</tr>");
                    }
                    out.println("</table>");
                }
                String error = (String) request.getAttribute("error");
                if (error != null) {
                    out.println("<br>");
                    out.println("<p>");
                    out.println(error);
                    out.println("<p/>");
                }
                String localTime = (String) request.getAttribute("localTime");
                String requestedCity = (String) request.getAttribute("requestedCity");
                if (localTime != null) {
                    out.println("<br>");
                    out.println("<p>");
                    out.println("Local time for the " + requestedCity + " city is: " + localTime);
                    out.println("<p/>");
                }

            %>
        <h3 align="left">Get local time for city</h3>
        <form name="Get time" action="GetLocalTimeServlet" method="POST">            
            City for which to display local time:<br/>
            <input type="text" name="city" value="" size="20" />
            <br/>
            <input type="submit" value="Get time" />            
        </form>
        <form name="List Flights" action="ListFlightsClientServlet" method="POST">
            <input type="submit" value="List Flights" name="List Flights Button" />
        </form>
        <form name="Log out button" action="LogoutServlet" method="POST">
            <input type="submit" value="Log Out" name="Log Out Button" />
        </form>
    </body></html>