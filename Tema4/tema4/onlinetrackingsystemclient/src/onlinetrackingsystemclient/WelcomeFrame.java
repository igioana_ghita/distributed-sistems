/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package onlinetrackingsystemclient;

import javax.swing.JFrame;
import tracking.login.User;


/**
 *
 */
public class WelcomeFrame extends JFrame {
    private PackageHandleFrame frame;
    
            
    /**
     * Creates new form WelcomeFrame
     */
    public WelcomeFrame() {       
        initComponents();   
        setSize(400, 400);
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        titleLabel = new javax.swing.JLabel();
        usernameLabel = new javax.swing.JLabel();
        passwordLabel = new javax.swing.JLabel();
        usernameTextField = new javax.swing.JTextField();
        passwordTextField = new javax.swing.JTextField();
        loginButton = new javax.swing.JToggleButton();
        registerButton = new javax.swing.JToggleButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("OTS");
        getContentPane().setLayout(null);

        titleLabel.setText("OnlineTrackingSystem");
        getContentPane().add(titleLabel);
        titleLabel.setBounds(90, 19, 140, 30);

        usernameLabel.setText("Username");
        getContentPane().add(usernameLabel);
        usernameLabel.setBounds(30, 70, 60, 14);

        passwordLabel.setText("Password");
        getContentPane().add(passwordLabel);
        passwordLabel.setBounds(30, 100, 60, 14);
        getContentPane().add(usernameTextField);
        usernameTextField.setBounds(110, 70, 100, 20);
        getContentPane().add(passwordTextField);
        passwordTextField.setBounds(110, 100, 100, 20);

        loginButton.setText("login");
        loginButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loginButtonActionPerformed(evt);
            }
        });
        getContentPane().add(loginButton);
        loginButton.setBounds(40, 160, 130, 23);

        registerButton.setText("register");
        registerButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registerButtonActionPerformed(evt);
            }
        });
        getContentPane().add(registerButton);
        registerButton.setBounds(190, 160, 110, 23);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void loginButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loginButtonActionPerformed
        int result = checkLogin(usernameTextField.getText(), passwordTextField.getText());
        System.out.println(result);
        if(result < 0){
            return;            
        }
        User user = getUserId(usernameTextField.getText(), passwordTextField.getText());              
        frame.setLoggedInUser(user.getId());
        frame.redo(user.getType());
        frame.setVisible(true);
        this.setVisible(false);
        usernameTextField.setText("");
        passwordTextField.setText("");
    }//GEN-LAST:event_loginButtonActionPerformed

    private void registerButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registerButtonActionPerformed
        registerUser(usernameTextField.getText(), passwordTextField.getText());        
        User user = getUserId(usernameTextField.getText(), passwordTextField.getText());      
        frame.setLoggedInUser(user.getId());
        frame.redo(user.getType());
        frame.setVisible(true);
        this.setVisible(false);
        usernameTextField.setText("");
        passwordTextField.setText("");
    }//GEN-LAST:event_registerButtonActionPerformed

  

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton loginButton;
    private javax.swing.JLabel passwordLabel;
    private javax.swing.JTextField passwordTextField;
    private javax.swing.JToggleButton registerButton;
    private javax.swing.JLabel titleLabel;
    private javax.swing.JLabel usernameLabel;
    private javax.swing.JTextField usernameTextField;
    // End of variables declaration//GEN-END:variables

    void setChangingFrame(PackageHandleFrame phFrame) {
        this.frame = phFrame;
    }
    
    private static int checkLogin(java.lang.String username, java.lang.String password) {
        tracking.login.TrakingLoginService_Service service = new tracking.login.TrakingLoginService_Service();
        tracking.login.TrakingLoginService port = service.getTrakingLoginServicePort();
        return port.checkLogin(username, password);
    }

    private static User getUserId(java.lang.String username, java.lang.String password) {
        tracking.login.TrakingLoginService_Service service = new tracking.login.TrakingLoginService_Service();
        tracking.login.TrakingLoginService port = service.getTrakingLoginServicePort();
        return port.getUserId(username, password);
    }

    private static int registerUser(java.lang.String username, java.lang.String password) {
        tracking.login.TrakingLoginService_Service service = new tracking.login.TrakingLoginService_Service();
        tracking.login.TrakingLoginService port = service.getTrakingLoginServicePort();
        return port.registerUser(username, password);
    }
}
