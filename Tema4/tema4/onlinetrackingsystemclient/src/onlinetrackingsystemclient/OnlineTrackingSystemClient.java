/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package onlinetrackingsystemclient;

/**
 *
 */
public class OnlineTrackingSystemClient {

    private PackageHandleFrame phFrame;
    private WelcomeFrame wFrame;

    public OnlineTrackingSystemClient() {
        phFrame = new PackageHandleFrame();
        phFrame.setVisible(false);
        wFrame = new WelcomeFrame();
        wFrame.setVisible(true);
        phFrame.setChangingFrame(wFrame);
        wFrame.setChangingFrame(phFrame);        
    }
    
    
            
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new OnlineTrackingSystemClient();
    }
    
}
