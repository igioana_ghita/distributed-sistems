/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tracking.login;

import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import tracking.dao.UserDao;
import tracking.entities.User;

/**
 *
 */
@WebService(serviceName = "TrakingLoginService")
@HandlerChain(file = "TrakingLoginService_handler.xml")
public class TrakingLoginService {

    @EJB
    private UserDao dao;
           
    @WebMethod(operationName = "checkLogin")
    public int checkLogin(@WebParam(name = "username") String username, @WebParam(name="password") String password) {
        List<User> usersList =  dao.getLoginUsers(username, password);        
        if(usersList.isEmpty()){
            return -1;
        }
        if(usersList.size()>1){
            return -2;
        }
        User identity = usersList.get(0);
        System.out.println("login user:"+identity);
        return identity.getType();        
    }
    
    @WebMethod(operationName = "registerUser")
    public int registerUser(@WebParam(name = "username") String username, @WebParam(name="password") String password) {
        dao.addUser(username, password);                
        return 0;        
    }
    
    @WebMethod(operationName = "getUserId")
    public User getUser(@WebParam(name = "username") String username, @WebParam(name="password") String password) {
        List<User> usersList =  dao.getLoginUsers(username, password);        
        if(usersList.isEmpty()){
            return null;
        }
        if(usersList.size()>1){
            return null;
        }
        User identity = usersList.get(0);
        System.out.println("login user: "+identity);
        return identity;
    }
}
