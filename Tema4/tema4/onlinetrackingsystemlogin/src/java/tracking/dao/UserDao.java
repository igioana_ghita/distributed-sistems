/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tracking.dao;

import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tracking.entities.User;
import java.util.logging.Logger;

/**
 *
 */
@Stateless
@LocalBean
public class UserDao {

    private static final Logger LOGGER = Logger.getLogger("UserDao");

    @PersistenceContext(unitName = "OnlineTrackingSystemLoginPU")
    EntityManager em;

    public User getUser(int id) {
        System.out.println("Get user: " + id);
        User user = em.find(User.class, id);
        System.out.println("Found user: " + user);        
        return user;        
    }

    public List<User> getLoginUsers(String username, String password) {
        TypedQuery<User> qu = em.createNamedQuery("User.findByUsernameAndPassword", User.class);
        qu.setParameter("username", username);
        qu.setParameter("password", password);
        List<User> users = qu.getResultList();
        System.out.println("get Login Users: " + users);        
        return users;
    }

    public List<User> getUsers() {
        TypedQuery<User> qb = em.createNamedQuery("User.findAll", User.class);
        List<User> users = qb.getResultList();
        System.out.println("get all users: " + users);
        return users;
    }

    public void addUser(String username, String password) {
        User user = new User(null, username, password, 2);
        em.persist(user);
        System.out.println("Add user: " + user);                
    }

}
