/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tracking.dao;

import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tracking.entities.Package;
import java.util.logging.Logger;

/**
 *
 */
@Stateless
@LocalBean
public class PackageDao {

    private static final Logger LOGGER = Logger.getLogger("UserDao");

    @PersistenceContext(unitName = "OnlineTrackingSystemPackageStatusPU")
    EntityManager em;

    public Package getPackage(int id) {
        Package pack = em.find(Package.class, id);
        System.out.println("Found package: " + pack);        
        return pack;        
    }

    public List<Package> getCustomerPackages(int customerId) {
        em.getEntityManagerFactory().getCache().evictAll();
        TypedQuery<Package> qu = em.createNamedQuery("Package.findByCustomer", Package.class);
        qu.setParameter("customer", customerId);
        List<Package> packs = qu.getResultList();
        System.out.println("get packages for customer "+customerId+" : " + packs);        
        return packs;
    }
    
    public List<Package> getCustomerPackages() {
        em.getEntityManagerFactory().getCache().evictAll();
        TypedQuery<Package> qu = em.createNamedQuery("Package.findAll", Package.class);        
        List<Package> packs = qu.getResultList();
        System.out.println("get packages for customer : " + packs);        
        return packs;
    }

    public void addPackage(Package pack) {        
        em.persist(pack);        
        System.out.println("Add package: " + pack);        
    }
    
    public void removePackage(Package pack) {        
        em.remove(pack);
        System.out.println("Remove package: " + pack);        
    }
    
    public void updatePackage(Package pack) {        
        em.merge(pack);
        System.out.println("Update package: " + pack);        
    }

    public List<Package> getPackages() {
        TypedQuery<Package> qb = em.createNamedQuery("User.findAll", Package.class);
        List<Package> packs = qb.getResultList();
        System.out.println("get all packages: " + packs);
        return packs;
    }

}
