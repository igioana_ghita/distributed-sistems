/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tracking.status;

import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import tracking.dao.PackageDao;

import tracking.entities.Package;
import tracking.entities.Status;
import tracking.util.StatusMapConverter;

/**
 *
 */
@WebService(serviceName = "TrackingStatusService")
@HandlerChain(file = "TrackingStatusService_handler.xml")
public class TrackingStatusService {
    
    @EJB
    private PackageDao dao;
        
    @WebMethod(operationName = "updatePackageStatus")
    public int updatePackageStatus(@WebParam(name = "id") int id, @WebParam(name = "status") int status) {        
        tracking.entities.Package pack = dao.getPackage(id);
        pack.setStatus(status);
        dao.updatePackage(pack);
        return 0;
    }
    
    @WebMethod(operationName = "getCustomerPackages")
    public List<Package> getCustomerPackages(@WebParam(name = "customerId") int customerId) {            
        return dao.getCustomerPackages(customerId);
    }
    
    @WebMethod(operationName = "getCustomerPackagesAll")
    public List<Package> getCustomerPackagesAll() {            
        return dao.getCustomerPackages();
    }
    
    @WebMethod(operationName = "getStatuses")
    @WebResult(name="mapResult")
    public String getStatuses() {            
        System.out.println(Status.getMap());
        StatusMapConverter converter = new StatusMapConverter(Status.getMap());
        return converter.convert();
    }
}
