/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tracking.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 *
 */
public class StatusMapConverter implements Serializable{
    private final HashMap<String, Integer> aMap;
    private static final String KEY_SEPARATOR="-";
    private static final String VALUE_SEPARATOR="=";
    
    public StatusMapConverter(){
        aMap = new HashMap<>();
    }
    
    public StatusMapConverter(HashMap<String, Integer> anotherMap){
        aMap = anotherMap;                
    }
    
    public HashMap<String,Integer> getMap(){
        return aMap;
    }
    
    public String convert(){
        StringBuilder builder = new StringBuilder();
        for(Entry<String,Integer> entry : aMap.entrySet()){
            builder.append(entry.getKey()).append(VALUE_SEPARATOR).append(entry.getValue()).append(KEY_SEPARATOR);
        }
        builder.delete(builder.length()-1, builder.length());               
        return builder.toString();
    }
}
