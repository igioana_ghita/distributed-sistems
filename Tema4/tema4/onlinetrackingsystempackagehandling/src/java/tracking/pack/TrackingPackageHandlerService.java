/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tracking.pack;

import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import tracking.dao.PackageDao;
import tracking.entities.Package;
import tracking.entities.Status;

/**
 *
 */
@WebService(serviceName = "TrackingPackageHandlerService")
@HandlerChain(file = "TrackingPackageService_handler.xml")
public class TrackingPackageHandlerService {

    @EJB
    private PackageDao dao;

    @WebMethod(operationName = "addPackage")
    public int addPackage(@WebParam(name = "content") String content, @WebParam(name = "customerId") int customerId) {
        Package pack = new Package(null, content, Status.REGISTERED_ID, customerId);
        dao.addPackage(pack);
        return 0;
    }

    @WebMethod(operationName = "removePackage")
    public int removePackage(@WebParam(name = "id") int id) {
        Package pack = dao.getPackage(id);
        if (pack != null) {
            dao.removePackage(pack);
        }
        return 0;
    }

    @WebMethod(operationName = "registerPackageForTracking")
    public int registerPackageForTracking(@WebParam(name = "id") int id) {
        Package pack = dao.getPackage(id);
        if (pack != null) {
            pack.setStatus(Status.PROCESSING_ID);
            dao.updatePackage(pack);
        }
        return 0;
    }

}
