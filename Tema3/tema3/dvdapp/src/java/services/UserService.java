package services;

import dao.UserDao;
import java.security.SecureRandom;
import model.User;

public class UserService {

    private static final UserService INSTANCE = new UserService();
    
    private static final SecureRandom random = new SecureRandom();

    private UserService() {
        if (INSTANCE != null) {
            throw new IllegalStateException("Already instantiated");
        }
    }

    public static UserService getInstance() {
        return INSTANCE;
    }
    
    public User logInUser(User user){
        user.setLoggedInKey(String.valueOf(random.nextInt(100000)));
        return UserDao.getInstance().update(user);        
    }

    public User logOutUser(User user){
        user.setLoggedInKey("");
        return UserDao.getInstance().update(user);        
    }
}
