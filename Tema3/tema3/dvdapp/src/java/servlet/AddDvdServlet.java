package servlet;

import client.Producer;
import commons.DvdMessage;
import dao.DvdDao;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Dvd;
import model.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class AddDvdServlet extends BaseServlet {

    private static final Log LOGGER = LogFactory.getLog(AddDvdServlet.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOGGER.info("Get local time servlet called");
        handleProcessRequest(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    @Override
    protected void processCorrectRequest(User user, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter("title");
        String location = request.getParameter("location");
        Long price = Long.valueOf(request.getParameter("price"));

        request.setAttribute("sent", "yes");
        Producer producer = new Producer("queue");
        DvdMessage message = new DvdMessage(title, location, price);
        producer.sendMessage(message);
        LOGGER.info("received " + title + ":" + location + ":" + price);
        Dvd dvd = new Dvd();
        dvd.setLocation(location);
        dvd.setTitle(title);
        dvd.setPrice(price);
        DvdDao.getInstance().create(dvd);

        RequestDispatcher view = request.getRequestDispatcher("admin.jsp");
        view.forward(request, response);
    }

    @Override
    protected List<Long> permittedRoles() {
        return Arrays.asList(1l);
    }

}
