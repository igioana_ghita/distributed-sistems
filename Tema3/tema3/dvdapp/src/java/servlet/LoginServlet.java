package servlet;

import dao.DvdDao;
import java.util.List;

import dao.UserDao;
import javax.servlet.RequestDispatcher;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Dvd;

import model.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import services.UserService;

public class LoginServlet extends BaseServlet {

    private static final Log LOGGER = LogFactory.getLog(LoginServlet.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info("Login servlet called");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        if (username == null || username.trim().equals("") || password == null || password.trim().equals("")) {
            request.setAttribute("loginError", "Invalid fields");

            RequestDispatcher view = request.getRequestDispatcher("index.jsp");
            view.forward(request, response);
            return;
        }

        User user = UserDao.getInstance().findLogin(username, password);
        user = UserService.getInstance().logInUser(user);

        if (user == null) {
            LOGGER.info("user is null");

            request.setAttribute("loginError", "Invalid fields");

            RequestDispatcher view = request.getRequestDispatcher("index.jsp");
            view.forward(request, response);
            return;
        }

        if (user.getRole() == 1l) {
            setHttpSession(user, "admin.jsp", request, response);
            return;
        }

        if (user.getRole() == 2l) {            
            List<Dvd> dvds = DvdDao.getInstance().listDvds();
            request.setAttribute("dvds", dvds);
            
            setHttpSession(user, "client.jsp", request, response);
            return;
        }

        RequestDispatcher view = request.getRequestDispatcher("index.jsp");
        view.forward(request, response);
    }

    private void setHttpSession(User user, String page, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info("user " + user.getId() + " successfully logged in");

        HttpSession session = request.getSession();
        session.setAttribute("userId", user.getId());
        session.setAttribute("key", user.getLoggedInKey());

        RequestDispatcher view = request.getRequestDispatcher(page);
        view.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    @Override
    protected void processCorrectRequest(User user, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected List<Long> permittedRoles() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
