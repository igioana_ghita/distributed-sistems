package servlet;

import dao.UserDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class BaseServlet extends HttpServlet {

    private static final Log LOGGER = LogFactory.getLog(BaseServlet.class);

    protected void redirectToInvalidPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("loggedOut", "invalid request");
        RequestDispatcher view = request.getRequestDispatcher("index.jsp");
        view.forward(request, response);
    }

    protected void handleProcessRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("userId") != null) {
            User user = UserDao.getInstance().findUser((Long) session.getAttribute("userId"));
            if (user.getLoggedInKey().equals(session.getAttribute("key")) && permittedRoles().contains(user.getRole())) {

                processCorrectRequest(user, request, response);
            } else {
                LOGGER.error("invalid call");
                redirectToInvalidPage(request, response);
            }
        }
    }

    protected abstract void processCorrectRequest(User user, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

    protected abstract List<Long> permittedRoles();
}
