package servlet;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import services.UserService;

public class LogoutServlet extends BaseServlet {

    private static final Log LOGGER = LogFactory.getLog(LogoutServlet.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOGGER.info("Logout servlet called");
        handleProcessRequest(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    @Override
    protected void processCorrectRequest(User user, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        UserService.getInstance().logOutUser(user);
        
        HttpSession session = request.getSession();
        session.setAttribute("userId", null);
        session.setAttribute("key", null);

        request.setAttribute("loggedOut", "You have been logged out");

        RequestDispatcher view = request.getRequestDispatcher("index.jsp");
        view.forward(request, response);        
    }
    
    @Override
    protected List<Long> permittedRoles() {
        return Arrays.asList(1l, 2l);
    }

}
