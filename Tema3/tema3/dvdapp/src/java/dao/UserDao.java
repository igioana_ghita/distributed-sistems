package dao;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;
import model.User;

import java.util.List;
import org.hibernate.cfg.Configuration;

public class UserDao {

    private static final Log LOGGER = LogFactory.getLog(UserDao.class);

    private static UserDao INSTANCE = new UserDao();

    private UserDao() {
        if (INSTANCE != null) {
            throw new IllegalStateException("Already instantiated");
        }
    }
    public static UserDao getInstance() {
        return INSTANCE;
    }
    private final SessionFactory factory = new Configuration().configure().buildSessionFactory();

    public User update(User user) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(user);            
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return user;
    }

    @SuppressWarnings("unchecked")
    public User findUser(Long id) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<User> users = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM User WHERE id = :id");
            query.setParameter("id", id);
            users = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return users != null && !users.isEmpty() ? users.get(0) : null;
    }

    @SuppressWarnings("unchecked")
    public User findLogin(String username, String password) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<User> users = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM User WHERE username = :username and password = :password");
            query.setParameter("username", username);
            query.setParameter("password", password);
            users = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return users != null && !users.isEmpty() ? users.get(0) : null;
    }
}
