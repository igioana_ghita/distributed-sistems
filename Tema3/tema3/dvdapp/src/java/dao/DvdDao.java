package dao;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;
import model.Dvd;

import java.util.List;
import org.hibernate.cfg.Configuration;

public class DvdDao {

    private static final Log LOGGER = LogFactory.getLog(DvdDao.class);

    private static DvdDao INSTANCE = new DvdDao();

    private DvdDao() {
        if (INSTANCE != null) {
            throw new IllegalStateException("Already instantiated");
        }
    }
    public static DvdDao getInstance() {
        return INSTANCE;
    }
    private final SessionFactory factory = new Configuration().configure().buildSessionFactory();

   public Dvd create(Dvd dvd) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(dvd);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return dvd;
    }
   
    public List<Dvd> listDvds() {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Dvd> dvds = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Dvd");
            dvds = query.list();         
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return dvds;
    }

}
