<%@page import="model.Dvd"%>
<%@ page import="java.util.*" %>

<html><body>
<h1 align="center">Client JSP</h1>
<p>
<%
	
	    out.println("<br>Welcome to the client page<br>");
   

                List<Dvd> dvds = (List<Dvd>) request.getAttribute("dvds");
                if (dvds != null && !dvds.isEmpty()) {
                    out.println("<table>");
                    out.println("<tr>");
                    out.println("<th>Id</th>");
                    out.println("<th>Title</th>");
                    out.println("<th>Location</th>");
                    out.println("<th>Price</th>");
                    out.println("</tr>");
                    Iterator<Dvd> i = dvds.iterator();
                    while (i.hasNext()) {
                        Dvd f = i.next();
                        out.println("<tr>");
                        out.println("<td>" + f.getId() + "</td>");
                        out.println("<td>" + f.getTitle()+ "</td>");
                        out.println("<td>" + f.getLocation() + "</td>");
                        out.println("<td>" + f.getPrice()+ "</td>");
                        out.println("</tr>");
                    }
                    out.println("</table>");
                }
              
            
%>
<form name="Log out button" action="LogoutServlet" method="POST">
    <input type="submit" value="Log Out" name="Log Out Button" />
</form>
</body></html>