package commons;

import java.io.Serializable;

public class DvdMessage implements Serializable {

    private String title;
    private String location;
    private Long price;

    public DvdMessage() {
    }

    public DvdMessage(String title, String location, Long price) {
        this.title = title;
        this.location = location;
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "A new dvd is available in the location \'" + location + "\'" +
                " with the title \'" + title + '\'' +
                " with the price price=" + price + '.';
    }
}
