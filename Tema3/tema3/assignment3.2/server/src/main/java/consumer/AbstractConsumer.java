package consumer;

import java.io.IOException;
import commons.EndPoint;

import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.ShutdownSignalException;


/**
 * The endpoint that consumes messages off of the queue. Happens to be runnable.
 *
 * @author syntx
 */
public abstract class AbstractConsumer extends EndPoint implements Runnable, Consumer {

    private final String queueName;

    public AbstractConsumer(String endPointName) throws IOException {
        super(endPointName);
        queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, endPointName, "");
    }

    public void run() {
        try {
            channel.basicConsume(queueName, true, this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Called when consumer is registered.
     */
    public void handleConsumeOk(String consumerTag) {
        System.out.println("Consumer " + consumerTag + " registered");
    }

    public void handleCancel(String consumerTag) {
    }

    public void handleCancelOk(String consumerTag) {
    }

    public void handleRecoverOk(String consumerTag) {
    }

    public void handleShutdownSignal(String consumerTag, ShutdownSignalException arg1) {
    }
}