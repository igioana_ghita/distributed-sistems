package consumer.text;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import consumer.AbstractConsumer;
import org.apache.commons.lang.SerializationUtils;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Envelope;


/**
 * The endpoint that consumes messages off of the queue. Happens to be runnable.
 *
 * @author syntx
 */
public class TextFileConsumer extends AbstractConsumer {

    private static int number = 0;

    public TextFileConsumer(String endPointName) throws IOException {
        super(endPointName);
    }

    /**
     * Called when new message is available.
     */
    public void handleDelivery(String consumerTag, Envelope env,
                               BasicProperties props, byte[] body) throws IOException {
        PrintWriter writer = new PrintWriter(new File((number++)+".txt"));
        writer.write(String.valueOf(SerializationUtils.deserialize(body)));
        writer.close();

    }
}