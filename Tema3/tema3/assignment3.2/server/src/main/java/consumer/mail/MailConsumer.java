package consumer.mail;

import java.io.IOException;
import java.util.List;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Envelope;
import commons.DvdMessage;
import consumer.AbstractConsumer;
import database.GetConstructor;
import database.UserDao;
import org.apache.commons.lang.SerializationUtils;

public class MailConsumer extends AbstractConsumer {

    public MailConsumer(String endPointName) throws IOException {
        super(endPointName);
    }

    /**
     * Called when new message is available.
     */
    public void handleDelivery(String consumerTag, Envelope env,
                               BasicProperties props, byte[] body) throws IOException {
        //modify this
        DvdMessage dvdMessage = (DvdMessage) SerializationUtils.deserialize(body);
        MailService mailService = new MailService("stan.catalin.gabriel", "parola");
        List<String> emails = UserDao.getAllUsers();
        System.out.println(emails);
        for(String email : emails) {
            try {
                mailService.sendMail(email, "new dvd in system", dvdMessage.toString());
            } catch (Exception e) {
                System.out.println("you may have less secure application filter on");
            }
        }
    }

}