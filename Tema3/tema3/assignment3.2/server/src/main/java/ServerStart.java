import consumer.mail.MailConsumer;
import consumer.text.TextFileConsumer;

import java.io.IOException;

public class ServerStart {
    public static void main(String[] args) throws IOException {
        MailConsumer mailConsumer = new MailConsumer("queue");
        Thread consumerThread1 = new Thread(mailConsumer);
        consumerThread1.start();
        TextFileConsumer consumer2 = new TextFileConsumer("queue");
        Thread consumerThread2 = new Thread(consumer2);
        consumerThread2.start();
    }
}
