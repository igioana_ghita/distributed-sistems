package database;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 */
public class UserDao {

    public static List<String> getAllUsers() {
        GetConstructor get = new GetConstructor("user_entity");
        String query = get.createQuery();
        List<String> users = new ArrayList<>();
        try {
            ResultSet set = DatabaseAccessor.executeQuery(query);

            while (set.next()) {
                String u = set.getString("email");
                users.add(u);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return users;
    }

}
