package database;

import java.sql.*;

public class DatabaseAccessor {

    private static Connection connection;

    private DatabaseAccessor() {
    }

    public static boolean init() {
        return !(getConnection() == null);
    }

    private static Connection getConnection() {
        if (connection == null) {
            System.out.println("initializing database connection");
            try {
                connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/dvd",
                        "root", "root");
            } catch (SQLException e) {
                logException(e);
            }
        }
        return connection;
    }

    private static void logException(Exception e) {
        System.out.println("cannot connect to database. Reason: " + e.getMessage());
    }

    public static ResultSet executeQuery(String query) throws Exception {
        ResultSet rs;
        PreparedStatement stmt;
        System.out.println("Running query: "+query);
        try {
            stmt = getConnection().prepareStatement(query);
            rs = stmt.executeQuery();
        } catch (SQLException e) {
            throw new Exception("cannot execute query " + query, e);
        }
        return rs;
    }

}
