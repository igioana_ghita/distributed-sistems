package database;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class GetConstructor {

    private Map<String, String> values;
    private Map<String, List<Integer>> inValues;
    private String endDate;
    private String inValueOperator = "and";
    private String startDate;
    private String dateField;
    private String tableName;
    private String sortField;
    private String sortDirection;
    private Integer limit;

    public GetConstructor(String tableName) {
        this.tableName = tableName;
        values = new LinkedHashMap<>();
        inValues = new HashMap<>();
    }

    public String createQuery() {
        StringBuilder query = new StringBuilder();
        query.append("SELECT * FROM `").append(tableName).append("`");
        boolean andNeeded = false;
        if (!values.isEmpty() || !inValues.isEmpty()) {
            query.append(" WHERE ");
            for (String columnsString : values.keySet()) {
                query.append("`").append(columnsString).append("`='").append(values.get(columnsString)).append("' and ");
            }

            for (String columnsString : inValues.keySet()) {
                if (!inValues.get(columnsString).isEmpty()) {
                    query.append("`").append(columnsString).append("` in (");
                    List<Integer> ids = inValues.get(columnsString);
                    StringBuilder stringBuilder = new StringBuilder();
                    for (Integer integer : ids) {
                        stringBuilder.append(",").append(integer);
                    }
                    query.append(stringBuilder.toString().substring(1));
                    query.append(") ").append(inValueOperator).append(" ");
                }
            }
            query.delete(query.length() - 4, query.length());

            andNeeded = true;
        }

        if (endDate != null && startDate != null && dateField != null) {
            if (andNeeded) {
                query.append(" and ");
            }
            query.append("`").append(dateField).append("`").append(" BETWEEN ").append('\'')
                    .append(startDate).append('\'').append(" and ").append('\'').append(endDate).append('\'');
        }
        if (sortField != null && sortDirection != null) {
            query.append(" ORDER BY ").append(sortField).append(" ").append(sortDirection);
        }

        if (limit != null) {
            query.append(" LIMIT ").append(limit);
        }

        return query.toString();
    }

    public void addValue(String key, String value) {
        values.put(key, value);
    }

    public void addInValue(String key, List<Integer> value) {
        inValues.put(key, value);
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public void setSortDirection(String sortDirection) {
        this.sortDirection = sortDirection;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setDateField(String dateField) {
        this.dateField = dateField;
    }

    public void setInValueOperator(String inValueOperator) {
        this.inValueOperator = inValueOperator;
    }
}
