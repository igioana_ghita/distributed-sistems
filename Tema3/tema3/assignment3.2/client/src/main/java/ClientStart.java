import client.Producer;
import commons.DvdMessage;

import java.io.IOException;

public class ClientStart {
    public static void main(String[] args) throws IOException {
        Producer producer = new Producer("queue");

        for (int i = 0; i < 2; i++) {
            DvdMessage message = new DvdMessage();
            message.setLocation("this location");
            message.setPrice(11L);
            message.setTitle("this title");
            producer.sendMessage(message);
            System.out.println("Message Number " + i + " sent.");
        }
    }
}
