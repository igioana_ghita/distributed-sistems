import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ClientStart {

	public static void main (String[] args) {
		ITaxService taxService;
		try {
			View view = new View();
			Registry registry = LocateRegistry.getRegistry("localhost", 8889);
			
			taxService = (ITaxService)registry.lookup("rmi://localhost");
			double result=taxService.computeSellinPrice(new Car(2015, 2000, 8000));
			System.out.println("Result is :"+result);
 
			}catch (Exception e) {
				System.out.println("HelloClient exception: " + e);
				}
		}
}
