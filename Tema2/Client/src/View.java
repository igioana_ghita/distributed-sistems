import javax.swing.*;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class View {

	private JFrame frame;
	private JPanel panel;
	private JTextField year;
	private JTextField engineCap;
	private JTextField price;
	private JTextField result;
	private JButton calculateTax;
	private JButton calculateSellPrice;

	public View() {
		frame = new JFrame("Tax Service");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel = new JPanel();
		panel.setLayout(new GridLayout(2, 3));
		year = new JTextField("Year");
		engineCap = new JTextField("Engine Capacity");
		price = new JTextField("Price");
		result = new JTextField(10);
		calculateSellPrice = new JButton("Pret Vanzare");
		calculateTax = new JButton("Pret Taxa");

		GridBagConstraints c = new GridBagConstraints();

		c.gridx = 0;
		c.gridy = 0;
		panel.add(year, c);

		c.gridx = 1;
		c.gridy = 0;
		panel.add(engineCap, c);

		c.gridx = 2;
		c.gridy = 0;
		panel.add(price, c);

		c.weighty = 0.5;
		c.gridx = 0;
		c.gridy = 1;
		panel.add(calculateTax, c);

		c.weighty = 0.5;
		c.gridx = 1;
		c.gridy = 1;
		panel.add(calculateSellPrice, c);

		c.gridheight = 1;
		c.weighty = 0.5;
		c.gridx = 2;
		c.gridy = 1;
		panel.add(result, c);

		frame.setContentPane(panel);

		frame.setSize(420, 350);
		frame.setVisible(true);

		calculateSellPrice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ITaxService taxService;
				try {
					Registry registry = LocateRegistry.getRegistry("localhost", 8889);
					Car c = new Car(Integer.valueOf(year.getText()), 
							Integer.valueOf(engineCap.getText()), Double.valueOf(price.getText()));
					taxService = (ITaxService) registry.lookup("rmi://localhost");
					double result_computation = taxService.computeSellinPrice(c);
					result.setText(String.valueOf(result_computation));

				} catch (Exception e1) {
					JOptionPane.showMessageDialog(new JFrame(), e1.getClass() + " " + e1.getMessage());
				}

			}
		});

		calculateTax.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ITaxService taxService;
				try {
					Registry registry = LocateRegistry.getRegistry("localhost", 8889);

					Car c = new Car(Integer.valueOf(year.getText()), 
							Integer.valueOf(engineCap.getText()), Double.valueOf(price.getText()));
					taxService = (ITaxService) registry.lookup("rmi://localhost");
					double result_computation = taxService.computeTax(c);
					result.setText(String.valueOf(result_computation));

				} catch (Exception e1) {
					JOptionPane.showMessageDialog(new JFrame(), e1.getClass() + " " + e1.getMessage());
				}
			}
		});
	}

	
}
