import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ServerStart {
	
	private static final int PORT = 8889;

	public static void main (String[] argv) {
		   try {
			   Registry registry = LocateRegistry.createRegistry(PORT);
			   
			   TaxService service = new TaxService();			   		   
			   registry.rebind("rmi://localhost", service);

			   System.out.println("Server is ready.");
			   }catch (Exception e) {
				   System.out.println("Server failed: " + e);
				}
		   }
}
